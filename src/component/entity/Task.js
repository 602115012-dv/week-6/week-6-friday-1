class Task {
  constructor(name, status) {
    this.name = name;
    this.status = status;
  }
}

export default Task;