const Status = {
  TODO: "todo",
  DONE: "done",
  REMOVE: "remove"
}

export default Status;