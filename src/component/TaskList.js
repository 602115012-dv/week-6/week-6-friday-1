import React from 'react';
import Status from './entity/Status';

const TaskList = (props) => {
  const updateTask = (index, status) => {
    props.updateTask(index, status);
  }

  const renderTableData = () => {
    return props.state.taskList.map((task, index) => {
      switch (task.status) {
        case Status.TODO:
          return (
            <tr key={index + 1}>
              <td>{index + 1}</td>
              <td>{task.name}</td>
              <td >
                <button className="btn btn-success" 
                onClick={() => updateTask(index, Status.DONE)}>
                  Done
                </button>
              </td>
            </tr>
          );
          
        case Status.DONE:
          return (
            <tr key={index + 1}>
              <td>{index + 1}</td>
              <td style={{textDecoration: "line-through"}}>{task.name}</td>
              <td >
                <button className="btn btn-danger" 
                onClick={() => updateTask(index, Status.REMOVE)}>
                  Remove
                </button>
              </td>
            </tr>
          );

        default:
          return null;
      }
    })
  }

  return (
    <div className="col">
      <table className="table table-striped" style={{textAlign: "center"}}>
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
          </tr>
        </thead>
        <tbody>
          {renderTableData()}
        </tbody>
      </table>
    </div>
  );
}

export default TaskList;