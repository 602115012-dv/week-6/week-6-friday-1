import React from 'react';
import Task from './entity/Task';
import Status from './entity/Status';

class TaskInput extends React.Component {
  render() {
    return (
      <div className="col">
        <form>
          <div className="form-row">
            <div className="col-10">
              <input type="text" className="form-control" 
              id="taskName" placeholder="Task name"></input>
            </div>
            <div className="col-2">
              <button type="button" className="btn btn-primary" 
              style={{width: "100%"}} onClick={this.addTask}>
                Add
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }

  addTask = () => {
    let taskName = document.getElementById("taskName").value;
    if (taskName === "") {
      alert("Task name cannot be empty");
    } else {
      this.props.addTask(
        new Task(taskName, 
        Status.TODO, 
        this.props.taskKey
      ));
    }
  }
}

export default TaskInput