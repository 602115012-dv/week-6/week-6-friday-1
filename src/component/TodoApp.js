import React from 'react';
import TaskInput from './TaskInput';
import TaskList from './TaskList';
import Status from './entity/Status';

class TodoApp extends React.Component {
  state = {
    taskList: [],
  }

  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-6">
            <div className="row">
              <TaskInput 
                addTask={this.addTask}
                taskKey={this.state.taskKey}
              />
              <div className="w-100"></div>
              <TaskList 
                state={this.state}
                updateTask={this.updateTask}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }

  addTask = (task) => {
    let taskList = this.state.taskList;
    taskList.push(task);
    this.setState({
      taskList: taskList,
    });
  }

  updateTask = (index ,status) => {
    let taskList = [...this.state.taskList];
    
    switch (status) {
      case Status.DONE:
        taskList[index].status = status;
        this.setState({
          taskList: taskList
        });
        break;

      case Status.REMOVE:
        taskList.splice(index, 1);
        this.setState({
          taskList: taskList
        });
        break;

      default:
        return;
    }
  }
}

export default TodoApp;